defmodule EscritaMusical do
  def note(n) do
    Enum.at(~w(C C# D D# E F F# G G# A A# B), rem(n,12))
  end

  def octave(n) do
    rem(div(n, 12), 3)+2
    |> Integer.to_string()
  end

  def ascii_to_note(letter) do
    cond do
      letter == 32 -> "Z"
      true -> note(letter-33) <> octave(letter-33)
    end
  end

  def group_to_length(list) do
    Integer.to_string(length(list)) <> hd(list)
  end

  def main() do
    num_tests = IO.gets("")
    |> String.strip()
    |> String.to_integer()

    for _i <- 1..num_tests do
      IO.gets("")
      |> String.strip()
      |> String.to_charlist()
      |> Enum.map(&ascii_to_note/1)
      |> Enum.join(" ")
      |> IO.puts()

      IO.puts("")
    end
  end
end

EscritaMusical.main()
