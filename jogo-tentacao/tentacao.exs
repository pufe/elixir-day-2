defmodule Tentacao do
  def read_line() do
    IO.gets("")
    |> String.strip()
  end

  def read_answers(answers) do
    line = read_line()
    if Regex.match?(~r/^Resposta/, line) do
      [_whole_match, number, text] = Regex.run(~r/^Resposta (\d+): (.+)$/, line)
      Map.put(answers, number, text)
      |> read_answers()
    else
      [_whole_match, number] = Regex.run(~r/^Numero (\d+)! Certa Resposta!$/, line)
      {answers, number}
    end
  end

  def read_question() do
    line = read_line()
    if line == "Ole Ole Ola" do
      nil
    else
      [_whole_match, number, question] = Regex.run(~r/^Pergunta (\d+): (.+)$/, line)
      number = String.to_integer(number)
      {answers, correct_answer} = read_answers(%{})
      {number, "#{question} #{answers[correct_answer]}"}
    end
  end

  def main() do
    "Silvio Santos Vem Ai" = read_line()
    Stream.repeatedly(&read_question/0)
    |> Stream.take_while(&(&1))
    |> Enum.sort_by(fn {number, _text} -> number end)
    |> Enum.each(fn {_number, text} -> IO.puts(text) end)
  end

end

Tentacao.main()
