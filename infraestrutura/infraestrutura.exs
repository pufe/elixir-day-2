defmodule Infraestrutura do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def read_input() do
    num_machines = read_int()
    num_jobs = read_int()
    if num_machines>0 and num_jobs>0 do
      jobs = Enum.map(1..num_jobs, fn _i -> read_int() end)
      {num_machines, jobs}
    end
  end

  def solve({num_machines, jobs}) do
    ub = Enum.sum(jobs)
    lb = 0
    binary_search(lb, ub, num_machines, jobs)
  end

  def binary_search(lb, ub, _num_machines, _jobs) when lb+1 == ub do
    ub
  end

  def binary_search(lb, ub, num_machines, jobs) do
    mid = div(lb+ub, 2)
    if can_solve(mid, num_machines, jobs, 0) do
      binary_search(lb, mid, num_machines, jobs)
    else
      binary_search(mid, ub, num_machines, jobs)
    end
  end

  def can_solve(_max_time, _num_machines, [], _current) do
    true
  end

  def can_solve(_max_time, 0, _jobs, _current) do
    false
  end

  def can_solve(max_time, num_machines, [first_job | more_jobs], current) do
    #IO.inspect({max_time, num_machines, first_job, more_jobs, current}, charlists: :as_lists)
    if (current+first_job <= max_time) do
      can_solve(max_time, num_machines, more_jobs, first_job+current)
    else
      can_solve(max_time, num_machines-1, [first_job | more_jobs], 0)
    end
  end

  def main() do
    Stream.repeatedly(&read_input/0)
    |> Stream.take_while(&(&1))
    |> Stream.map(&solve/1)
    |> Enum.each(&IO.puts/1)
  end
end

Infraestrutura.main()
