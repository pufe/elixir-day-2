// requires MIDI.js

var notes = "C C# D D# E F F# G G# A A# B".split(" ");

function to_length(x) {
  console.log(x);
  return x.match(/^[0-9]+/) - 0;
}

function to_height(x) {
  octave = x.match(/[0-9]+$/) - 0;
  note = x.match(/[^0-9]/)
  if (note == "Z")
    return -1;
  else
    return notes.findIndex(function(i) { return i==note;})+12*octave+36;
}

function play(note, len) {
  console.log(note);
  if (note > 0) {
    MIDI.noteOn(0, note, 127, 0);
    MIDI.noteOff(0, note,  len*0.2);
  }
}

var sequence = "1E4 1D#4 1E4 1D#4 1E4 1B3 1D4 1C4 1A3 1E3 1A3 1C3 1E3 1A3 1B3 1E3 1G#3 1E3 1G#3 1B3 1C4 1E3 1A3 1E3 1E4 1D#4 1E4 1D#4 1E4 1B3 1D4 1C4 1A3 1E3 1A3 1C3 1E3 1A3 1B3 1E3 1G#3 1D3 1C4 1B3 1A3 1E3 1A3 1E4 1D#4 1E4 1D#4 1E4 1B3 1D4 1C4 1A3 1E3 1A3 1C3 1E3 1A3 1B3 1E3 1G#3 1E3 1G#3 1B3 1C4 1E3 1A3 1E3 1E4 1D#4 1E4 1D#4 1E4 1B3 1D4 1C4 1A3 1E3 1A3 1C3 1E3 1A3 1B3 1E3 1G#3 1D3 1C4 1B3 1A3 1E3 1A3 1B3 1C4 1D4 1E4 1E3 1C4 1G3 1F4 1E4 1D4 1G3 1B3 1F3 1E4 1D4 1C4 1F3 1B3 1F3 1D4 1C4 1B3 1E3 1E4 1E3 1E4 1E3 1E4 1E4 1D#4 1E4 1D#4 1E4 1B3 1D4 1C4 1A3 1E3 1A3 1C3 1E3 1A3 1B3 1E3 1G#3 1E3 1G#3 1B3 1C4 1E3 1A3 1E3 1E4 1D#4 1E4 1D#4 1E4 1B3 1D4 1C4 1A3 1E3 1A3 1C3 1E3 1A3 1B3 1E3 1G#3 1D3 1C4 1B3 1A3 1E3 1A3";
var sequence2 = "1G#4 1D#3 1G#4 1E2 1G2 1D#2 1F#2 4Z 1F#4 1F#2 1E2 4Z 1E2 1C3 1C2 1B2 1F#2 1A#2 4Z 1G#4 1A#2 1G2 1E4 1F#4 1F#2 1A#2 1C#3 1C#1";
var foo = "1F#4 1F#2 1F2 1A#2 1F#2 1D#2 1G#4 1C#3 1D#2 1F#2 1A#4 1G2 1C#2 1B4 1G#4 2D#2 1F#2 1Z 1D3 1F#2 1A2 1D#2 1G4 1C2 1C#2 1G#2 1D4 1C#1";
var bar = "2D4 1G2 1A2 1G#4 1A#2 1G#4 1F2 1F#4 1G#4 1D4 1E4 1D3 1E4 1E3 1C#1";

function execute(sequence) {
  sequence = sequence.split(" ");
  position = 0;

  interval = null;
  silence = 0;

  interval = setInterval(function() {
    if (position >= sequence.length)
      clearInterval(interval);
    else if (silence>0)
      --silence;
    else {
      silence = to_length(sequence[position]);
      play(to_height(sequence[position++]), silence--);
    }
  }, 200);
}

execute(sequence);
