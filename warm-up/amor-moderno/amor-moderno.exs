defmodule AmorModerno do
  def count_letters(line) do
    String.to_charlist(line)
    |> Enum.reduce({0,0}, fn letter, {total, upper} ->
      cond do
        letter in ?a..?z -> {total+1, upper}
        letter in ?A..?Z -> {total+1, upper+1}
        true -> {total, upper}
      end
    end)
  end

  def pick_answer({total, upper}) do
    cond do
      upper==0 -> "oi"
      upper*100<=total*5 -> "entendi"
      upper*100<=total*20 -> "eu gosto dessa musica"
      true -> "desculpe"
    end
  end

  def main() do
    Stream.repeatedly(fn -> IO.gets("") end)
    |> Stream.map(&String.strip/1)
    |> Stream.take_while(&(&1!="*"))
    |> Stream.map(&count_letters/1)
    |> Stream.map(&pick_answer/1)
    |> Enum.each(&IO.puts/1)
  end
end

AmorModerno.main()
