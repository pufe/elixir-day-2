defmodule Bebidas do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def read_str() do
    {:ok, [value]} = :io.fread('', '~s')
    List.to_string(value)
  end

  def test_case(_arg) do
    n = read_int()

    map = Enum.reduce(1..n, %{}, fn _i, acc ->
      drink = read_str()
      amount = read_int()
      amount = Map.get(acc, drink, 0)+amount
      Map.put(acc, drink, amount)
    end)

    Map.keys(map)
    |> Enum.sort
    |> Enum.each(fn drink ->
      IO.puts("#{drink} #{map[drink]}")
    end)

    IO.puts("")
  end

  def main() do
    num_tests = read_int()
    Enum.each(1..num_tests, &test_case/1)
  end
end

Bebidas.main()
