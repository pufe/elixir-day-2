defmodule Infraestrutura do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def read_test_case() do
    num_machines = read_int()
    num_jobs = read_int()

    if num_machines > 0 and num_jobs > 0 do
      job_list = Enum.map(1..num_jobs, fn _i ->
        read_int()
      end)
      {num_machines, job_list}
    end
  end

  def solve_test_case({1, job_list}) do
    Enum.sum(job_list)
  end

  def solve_test_case({num_machines, job_list}) do
    all_subsets(job_list)
    |> Enum.map(fn subset ->
      max(Enum.sum(subset), solve_test_case({num_machines-1, job_list -- subset}))
    end)
    |> Enum.min()
  end

  def all_subsets([]) do
    [[]]
  end

  def all_subsets([head | tail]) do
    all_subsets(tail)
    |> Enum.reduce([], fn x, acc -> [[head | x], x | acc] end)
  end

  def main() do
    Stream.repeatedly(&read_test_case/0)
    |> Stream.take_while(&(&1))
    |> Stream.map(&solve_test_case/1)
    |> Enum.each(fn x -> IO.puts(x) end)
  end
end

Infraestrutura.main()
