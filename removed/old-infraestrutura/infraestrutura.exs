defmodule Infraestrutura do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def read_test_case() do
    num_machines = read_int()
    num_jobs = read_int()

    if num_machines > 0 and num_jobs > 0 do
      job_list = Enum.map(1..num_jobs, fn _i ->
        read_int()
      end)
      {num_machines, job_list}
    end
  end

  def solve_test_case({num_machines, job_list}) do
    {answer, _memo} = search(num_machines, job_list, %{})
    answer
  end


  def search(_num_machines, [], memo) do
    {0, memo}
  end

  def search(1, job_list, memo) do
    {Enum.sum(job_list), memo}
  end

  def search(num_machines, job_list, memo) do
    if memo[{num_machines, job_list}] do
      {memo[{num_machines, job_list}], memo}
    else
      {value_list, memo} = all_subsets(job_list)
      |> Enum.reduce({[], memo}, fn subset, {list, memo} ->
        {value, memo} = search(num_machines-1, job_list -- subset, memo)
        value = max(Enum.sum(subset), value)
        {[value | list], memo}
      end)

      answer = Enum.min(value_list)
      {answer, Map.put(memo, {num_machines, job_list}, answer)}
    end
  end

  def all_subsets([]) do
    [[]]
  end

  def all_subsets([head | tail]) do
    all_subsets(tail)
    |> Enum.reduce([], fn x, acc -> [[head | x], x | acc] end)
  end

  def main() do
    Stream.repeatedly(&read_test_case/0)
    |> Stream.take_while(&(&1))
    |> Stream.map(&solve_test_case/1)
    |> Enum.each(fn x -> IO.puts(x) end)
  end
end

Infraestrutura.main()
