defmodule Despesas do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def search(available_bars, current_bar, graph, is_min, memo) do
    if memo[{available_bars, current_bar, is_min}] do
      {memo[{available_bars, current_bar, is_min}], memo}
    else
      calculate(available_bars, current_bar, graph, is_min, memo)
    end
  end

  def calculate(available_bars, current_bar, graph, is_min, memo) do
    {costs, new_memo} = Enum.map(available_bars, fn next_bar ->
      {graph[{current_bar, next_bar}], next_bar}
    end)
    |> Enum.filter(fn {cost, _next_bar} -> cost end)
    |> Enum.map(fn {cost, next_bar} ->
      if is_min, do: {cost, next_bar}, else: {0, next_bar}
    end)
    |> Enum.reduce({[], memo}, fn {cost, next_bar}, {costs, new_memo} ->
      {answer, new_memo} = search(available_bars -- [next_bar], next_bar, graph, not is_min, new_memo)
      {[cost+answer | costs], new_memo}
    end)

    answer = if is_min do
      Enum.min(costs, fn -> 0 end)
    else
      Enum.max(costs, fn -> 0 end)
    end

    {answer, Map.put(new_memo, {available_bars, current_bar, is_min}, answer)}
  end

  def main() do
    num_bars = read_int()
    num_edges = read_int()

    graph = Enum.reduce(1..num_edges, %{}, fn _i, acc ->
      origin = read_int()
      destination = read_int()
      cost = read_int()
      Map.put(acc, {origin, destination}, cost)
    end)

    {cost, _memo} = search(Enum.to_list(2..num_bars), 1, graph, true, %{})

    IO.puts(cost)
  end
end

Despesas.main()
