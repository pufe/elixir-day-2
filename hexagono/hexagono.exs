defmodule Hexagono do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def main() do
    Enum.each(1..read_int(), fn _x ->
      IO.puts(2*read_int()-1)
    end)
  end
end

Hexagono.main()
