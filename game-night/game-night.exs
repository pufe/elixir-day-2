defmodule GameNight do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def debug(people, dp) do
    Enum.each(0..people, fn x -> :io.format(" ~3B", [dp[x]]) end)
    IO.puts("")
  end

  def solve([], _people, dp) do
    #debug(people, dp)
    dp
  end

  def solve([{kmin, kmax} | tail], people, dp) do
    #debug(people, dp)

    new_dp = Enum.reduce(1..people, [dp[0]], fn x, acc ->
      [dp[x]-dp[x-1]+find_interval(dp, x-kmax, x-kmin)+hd(acc) | acc]
    end)
    |> Enum.reverse()
    |> Enum.with_index()
    |> Enum.map(fn {a, b} -> {b, a} end)
    |> Enum.into(%{})

    solve(tail, people, new_dp)
  end

  def find_interval(dp, min, max) do
    cond do
      max < 0 -> 0
      min < 0 -> find_interval(dp, 0, max)
      min == 0 -> dp[max]
      true -> dp[max]-dp[min-1]
    end
  end

  def initial_dp(people) do
    Enum.reduce(0..people, %{}, fn x, acc ->
      Map.put(acc, x, 1)
    end)
  end

  def test_case(_test_number) do
    num_games = read_int()
    people = read_int()
    games = Enum.map(1..num_games, fn _x ->
      {read_int(), read_int()}
    end)
    dp = solve(games, people, initial_dp(people))
    if dp[people]>dp[people-1] do
      IO.puts("Ok!")
    else
      IO.puts("Corre, Pufe!")
    end
  end

  def main() do
    num_tests = read_int()
    Enum.each(1..num_tests, &test_case/1)
  end
end

GameNight.main()
