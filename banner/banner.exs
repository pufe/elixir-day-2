defmodule Banner do
  def read_line do
    IO.gets('') |> String.strip()
  end

  def test_case(_arg) do
    num_lines = String.to_integer(read_line())
    lines = Enum.map(1..num_lines, fn _x ->
      read_line()
    end)
    height = num_lines*32-2
    width = lines
    |> Enum.map(&String.length/1)
    |> Enum.max()
    |> Kernel.*(20)
    IO.puts("#{height} #{width}")
  end

  def main() do
    num_tests = String.to_integer(read_line())
    Enum.each(1..num_tests, &test_case/1)
  end
end

Banner.main()
