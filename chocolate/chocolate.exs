defmodule Chocolate do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def write_output(value) do
    :io.fwrite('~.2f~n', [value])
  end

  def read_drawer(m) do
    Enum.reduce(1..m, %{:total => 0}, fn x, acc ->
      current = read_int()
      Map.merge(acc, %{:total => acc[:total]+current, x => current})
    end)
  end

  def test_case(_arg) do
    n = read_int()
    m = read_int()
    storage = Enum.map(1..n, fn _x ->
      read_drawer(m)
    end)
    q = read_int()
    Enum.reduce(storage, 0, fn drawer, acc ->
      acc+(drawer[q]/drawer[:total])/n
    end)
    |> write_output()
  end

  def main() do
    num_tests = read_int()
    Enum.each(1..num_tests, &test_case/1)
  end
end

Chocolate.main()
