defmodule Duelo do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def read_str() do
    {:ok, [char_list]} = :io.fread('', '~s')
    List.to_string(char_list)
  end

  def read_package() do
    cost = read_int()
    num_duelos = read_int()
    duelos = Enum.reduce(1..num_duelos, MapSet.new, fn _i, acc ->
      MapSet.put(acc, read_str())
    end)

    %{cost: cost, duelos: duelos}
  end

  def read_input() do
    num_packages = read_int()
    packages = Enum.reduce(1..num_packages, [], fn _i, acc ->
      [read_package() | acc]
    end)
    universe = all_duelos(packages)

    {packages, universe}
  end

  def all_subsets([]) do
    [[]]
  end

  def all_subsets([head | tail]) do
    Enum.reduce(all_subsets(tail), [], fn subset, acc ->
      [ [head|subset], subset | acc ]
    end)
  end

  def min_cost(packages, universe) do
    all_subsets(packages)
    |> Enum.filter(fn subset -> cover(subset, universe) end)
    |> Enum.map(fn subset -> total_cost(subset) end)
    |> Enum.min()
  end

  def all_duelos(subset) do
    Enum.map(subset, &(Map.get(&1, :duelos)))
    |> Enum.reduce(MapSet.new, &MapSet.union/2)
  end

  def cover(subset, universe) do
    MapSet.equal?(all_duelos(subset), universe)
  end

  def total_cost(subset) do
    Enum.map(subset, &(Map.get(&1, :cost)))
    |> Enum.sum()
  end

  def main() do
    num_tests = read_int()
    Enum.each(1..num_tests, fn _i ->
      {packages, universe} = read_input()
      cost = min_cost(packages, universe)
      IO.puts(cost)
    end)
  end
end

Duelo.main()
