defmodule Firewall do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def read_str() do
    {:ok, [value]} = :io.fread('', '~s')
    List.to_string(value)
  end

  def main() do
    n = read_int()
    regex = ~r{^(?<proto>[^:]*://)?(?<user>[^/@]*@)?(?<domain>[^/?]+).*$}
    Enum.map(1..n, fn _x -> read_str() end)
    |> Enum.map(fn str -> Regex.named_captures(regex, str) end)
    |> Enum.map(fn %{"domain" => domain} -> domain end)
    |> Enum.sort()
    |> Enum.uniq()
    |> Enum.each(fn domain -> IO.puts(domain) end)
  end
end

Firewall.main()
