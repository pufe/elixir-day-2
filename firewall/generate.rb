protos = ["https://", "http://", "my-custom://", "sip://"]
auths = ["pufe@", "pufe:pufe@", "we:rd@", "1234@"]
domains = ["querobolsa.com.br", "revistaqb.com.br", "quero.education", "bad.horse"]
paths = ["/", "/dev/null", "/1234", "/~pufe", "/:"]
queries = ["?q=123", "?a=1&b=2"]
100.times do
   print protos.shuffle.first if rand<0.7
   print auths.shuffle.first if rand<0.7
   print domains.shuffle.first
   print paths.shuffle.first if rand<0.7
   print queries.shuffle.first if rand<0.7
   puts
   end
