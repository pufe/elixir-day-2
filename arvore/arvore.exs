defmodule FastArvores do
  use Bitwise

  def is_power_of_two(n) do
    n == (n &&& -n)
  end

  def highest_power_of_two(n, x) do
    if 2*x>n do
      x
    else
      highest_power_of_two(n, 2*x)
    end
  end

  def height(n) do
    cond do
      n==2 -> 0
      is_power_of_two(n) -> 1 + height(div(n, 2))
      true -> height(n-highest_power_of_two(n, 1)+1)
    end
  end

  def solve(_test_number) do
    {:ok, [n]} = :io.fread('', '~d')
    IO.puts(height(n+2))
  end

  def main() do
    {:ok, [num_tests]} = :io.fread('', '~d')
    Enum.each(1..num_tests, &solve/1)
  end
end

defmodule SlowArvores do
  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def fill_list(position, list, size, limit) do
    if limit>=size do
      fill_list(position+1, [list, list, position], 2*size+1, limit)
    else
      List.flatten(list)
    end
  end

  def main() do
    num_tests = read_int()
    test_cases = Enum.map(1..num_tests, fn _x -> read_int() end)
    limit = Enum.max(test_cases)
    precompute = fill_list(0, [], 0, limit)
    Enum.each(test_cases, fn x -> IO.puts(Enum.at(precompute, x)) end)
  end
end

defmodule RecursiveArvores do
  def search_tree(target, current, height) do
    cond do
      current == target -> {:found, height}
      height == 0 -> {:not_found, current}
      true ->
        case search_tree(target, current-1, height-1) do
          {:found, x} -> {:found, x}
          {:not_found, current} -> search_tree(target, current-1, height-1)
        end
    end
  end

  def solve(n) do
    {:found, height} = search_tree(n, 131070, 16)
    height
  end

  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def test_case(_arg) do
    read_int()
    |> solve()
    |> IO.puts()
  end

  def main() do
    num_tests = read_int()
    Enum.each(1..num_tests, &test_case/1)
  end
end

defmodule FastRecursiveArvores do
  def search_tree(target, current, height) do
    if current == target do
      height
    else
      current = div(current, 2)
      target = rem(target, current)
      height = height-1
      current = current-1
      search_tree(target, current, height)
    end
  end

  def solve(n) do
    search_tree(n, 131070, 16)
  end

  def read_int() do
    {:ok, [value]} = :io.fread('', '~d')
    value
  end

  def test_case(_arg) do
    read_int()
    |> solve()
    |> IO.puts()
  end

  def main() do
    num_tests = read_int()
    Enum.each(1..num_tests, &test_case/1)
  end
end

SlowArvores.main()
